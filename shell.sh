#!/bin/bash
#1 Exercice  utiliser la commande man mkdir
man mkdir
#2 Exercice utiliser la commande -p avec mkdir pour créer les reps en imbriqués
sudo mkdir -p /SAKINA/FORMATION_CDA/rep-toto/rep-titi/rep-tata
#3 Exercice utiliser la commande touch pour créer un fichier 
sudo touch SAKINA/FORMATION_CDA/rep-toto/toto
sudo touch SAKINA/FORMATION_CDA/rep-toto/rep-titi/titi
sudo touch SAKINA/FORMATION_CDA/rep-toto/rep-titi/rep-tata/tata
#4 Exercice utiliser la commande ln -s pour faire un symlink
sudo ln -s rep-toto 42
#5 commande ls -la pour lister les informations
sudo ls -la 

